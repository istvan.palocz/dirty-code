let tomb = [];

for (let i = 0; i < 30; i++) {
    tomb[i] = i;
}

let paros = 0;
let osszeg = 0;
let hettelOszthato = false;
let parosIndex = [];
let k = 0;
let max = tomb[0];
let min = tomb[0];
let parosSzamok = [];

for (let i = 0; i < tomb.length; i++) {
    if (tomb[i] % 2 == 1) {
        paros++;
        parosIndex[k] = i;
        k++;
    }
    osszeg += tomb[i];
    if (tomb[i] % 7 == 0) {
        hettelOszthato = true;
    }
    if (tomb[i] > max) {
        max = tomb[i];
    }
    if (tomb[i] < min) {
        min = tomb[i];
    }
}

let j = 0;
for (let i = 0; i < tomb.length; i++) {
    if (tomb[i] % 2 == 0) {
        parosSzamok[j] = tomb[i];
        j++;
    }
}

console.log("Páros számok száma: " + paros);
console.log(`A számok összege: ${osszeg}`);
console.log(`Van e benne 7-tel osztható szám? ${hettelOszthato}`);
console.log(`A páros számok indexei: ${parosIndex}`)
console.log(`A legnagyobb szám: ${max}`);
console.log(`A legkisebb szám: ${min}`);
console.log(`Páros számok: ${parosSzamok}`);
