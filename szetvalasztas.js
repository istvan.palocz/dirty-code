let tomb = [3,7,4,2,9,3,6,7];
function kartyasit(fejlec,tartalom)
{
    console.log(`${fejlec}\n\t${tartalom}`)
    return `
        <div class="kartya">
            <div>
                ${fejlec}
            </div>
            <div>
                ${tartalom}
            </div>
        </div>
    `
}
function parosdarab(){
    let darab = 0;
    for(let elem of tomb){
        if (elem % 2 == 0){
            darab++;
        }
    }
    return kartyasit("1 Párosok",`Páros számok száma: ${darab}`);
}

function szamokosszege(){
    let darab = 0;
    for(let elem of tomb){
        darab += elem;
    }
    return kartyasit("2. Összeg",`Számokösszege: ${darab}`);
}

function hetteloszhato(){
    let vane = "nincs";
    for(let elem of tomb){
       if(elem % 7 == 0){
           vane = "van"
       }
    }
    return kartyasit("3. Héttel oszthatók",`A tömbben ${vane} 7-tel osztható szám.`);
}

function parosindexek(){
    let indexek="";
    for(i = 0; i < tomb.length; i++){
        if(tomb[i] % 2 == 0){
            indexek += i+". "; 
        }
    }
    return kartyasit("4. Párosok indexei",`Indexek: ${indexek}`);
}

function maximum(){
    let max = -Infinity;
    for(let elem of tomb){
        if(elem > max){
            max = elem;
        }
    }
    return kartyasit("5. Maximum",`Legnagyobb szám: ${max}`);
}

function minimum(){
    let min = Infinity;
    for(let elem of tomb){
        if(elem < min){
            min = elem;
        }
    }
    return kartyasit("6. Minimum",`Legkissebb szám: ${min}`);
}

function parosszamok(){
    let indexek="";
    for(let elem of tomb){
        if(elem % 2 == 0){
            indexek += elem+" "; 
        }
    }
    return kartyasit("7. Párosok számok",`A számok: ${indexek}`);
}

function csokrosit(){
    return parosdarab() + szamokosszege() + hetteloszhato() + parosindexek() + maximum() + minimum() + parosszamok();
}

function init(){
    szoveg = csokrosit();
    document.getElementById("tartalom").innerHTML = szoveg;
}
